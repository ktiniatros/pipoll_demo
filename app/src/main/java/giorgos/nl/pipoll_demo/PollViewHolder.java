package giorgos.nl.pipoll_demo;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by giorgos on 05/07/16.
 */
public class PollViewHolder {
    @BindView(R.id.date)
    TextView date;

    @BindView(R.id.clockIcon)
    ImageView clockIcon;

    @BindView(R.id.daysLeft)
    TextView daysLeft;

    @BindView(R.id.subject)
    TextView subject;

    @BindView(R.id.linkButton)
    Button linkButton;

    @BindView(R.id.lockButton)
    RelativeLayout lockButton;

    @BindView(R.id.shareButton)
    RelativeLayout shareButton;

    @BindView(R.id.counter)
    TextView counter;

    @BindView(R.id.extraButtonsContainer)
    RelativeLayout extraButtonsContainer;

    @BindView(R.id.extraButtonsContainerOfContainer)
    RelativeLayout extraButtonsContainerOfContainer;

    @BindView(R.id.pollAnswerButtonsContainer)
    LinearLayout pollAnswerButtonsContainer;

    @BindView(R.id.media)
    ImageView media;

    @BindView(R.id.tagsContainer)
    LinearLayout tagsContainer;

    @BindView(R.id.bookMarkButton)
    Button bookMarkButton;

    @BindView(R.id.lockImage)
    ImageView lockImage;

    public PollViewHolder(View view) {
        ButterKnife.bind(this, view);
    }
}
