package giorgos.nl.pipoll_demo;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import org.json.JSONException;
import org.json.JSONObject;

import giorgos.nl.pipoll_demo.R;

/**
 * Created by giorgos on 05/07/16.
 */
public class WebViewActivity extends AppCompatActivity {
    private static final String TAG = "pWEBVIEW";
    private WebView webview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.webview);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(getResources().getDrawable(R.mipmap.back));

        webview = (WebView)findViewById(R.id.webview);

        String title = getIntent().getStringExtra("title");
        actionBar.setTitle(title);

        String url = getIntent().getStringExtra("url");

        webview.setWebViewClient(new WebViewClient());
        webview.getSettings().setJavaScriptEnabled(true);
        webview.loadUrl(url);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return false;
    }
}
