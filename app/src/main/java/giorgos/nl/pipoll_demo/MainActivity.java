package giorgos.nl.pipoll_demo;

import android.content.SharedPreferences;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.widget.ListView;

import com.google.gson.JsonObject;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import giorgos.nl.pipoll_demo.models.Poll;
import giorgos.nl.pipoll_demo.presenters.PollsPresenter;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener{

    private static final String TAG = "pMainActivity";
    private PollsAdapter adapter;
    private PollsPresenter pollsPresenter;

    @BindView(R.id.list)
    ListView list;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    private Snackbar snack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        SharedPreferences.Editor appSettingsEditor = getSharedPreferences("settings", 0).edit();
        //for demo purposes, just use my demo account always
        appSettingsEditor.putInt("userId", 38).commit();
        snack = Snackbar.make(findViewById(android.R.id.content), R.string.loading, Snackbar.LENGTH_INDEFINITE);

        int dp = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 1, getResources().getDisplayMetrics());
        adapter = new PollsAdapter(this, R.layout.poll_item, 40 * dp, dp);
        list.setAdapter(adapter);
        pollsPresenter = new PollsPresenter(this, new PollsService());
        swipeRefreshLayout.setOnRefreshListener(this);
    }

    @Override
    protected void onResume(){
        super.onResume();
        onRefresh();
    }

    public void refreshPollsList(List<Poll> polls){
        if(polls.size() > 1){
            for (int i = 0, j = polls.size(); i < j; i++) {
                try {
                    adapter.add(polls.get(i));
                } catch (Exception jsonE) {
                    jsonE.printStackTrace();
                }
            }
            adapter.notifyDataSetChanged();
            swipeRefreshLayout.setRefreshing(false);
            snack.dismiss();
        }
    }

    public void onRefresh(){
        snack.show();
        pollsPresenter.refresh();
    }
}
