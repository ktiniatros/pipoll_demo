package giorgos.nl.pipoll_demo;

import android.content.Context;
import android.content.Intent;
import android.text.InputType;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.ButterKnife;
import giorgos.nl.pipoll_demo.models.Poll;

/**
 * Created by giorgos on 05/07/16.
 */
public class PollsAdapter extends ArrayAdapter<Poll> {

    private final String TAG = "pPollAdapter";
    private final Context context;
    private int buttonHeight = 0;
    private int dp = 1;

    public PollsAdapter(Context context, int textViewResourceId, int buttonHeight, int dp) {
        super(context, textViewResourceId);
        this.context = context;
        this.buttonHeight = buttonHeight;
        this.dp = dp;
    }

    public void fillRow(RelativeLayout container, final View filler, final float percentage, int color) {

        filler.setBackgroundColor(color);
        filler.animate().scaleX(0).setDuration(1).withEndAction(new Runnable() {
            @Override
            public void run() {
                filler.setAlpha(1.0f);
                filler.animate().scaleX(percentage).setDuration(1000);
            }
        });

        TextView perc = new TextView(container.getContext());
        RelativeLayout.LayoutParams percParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        percParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
        percParams.addRule(RelativeLayout.CENTER_VERTICAL, RelativeLayout.TRUE);
        percParams.setMargins(0, 0, 2 * dp, 0);
        perc.setLayoutParams(percParams);
        perc.setText(Math.round(percentage * 100) + "%");
        perc.setTextColor(this.context.getResources().getColor(R.color.smallTextGrey));
        perc.setAlpha(0);
        container.addView(perc);
        perc.animate().alpha(1).setDuration(2000);
    }

    public void emptyRow(RelativeLayout container, View filler) {
        filler.setAlpha(0);
        container.removeViewAt(container.getChildCount() - 1);
    }

    @Override
    public View getView(final int position, View rowView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService
                (Context.LAYOUT_INFLATER_SERVICE);

        final PollViewHolder holder;

        if (rowView != null) {
            holder = (PollViewHolder) rowView.getTag();
        } else {
            rowView = inflater.inflate(R.layout.poll_item, null, true);
            holder = new PollViewHolder(rowView);
            rowView.setTag(holder);
        }

        TextView date = holder.date;
        ImageView clockIcon = holder.clockIcon;
        TextView daysLeftView = holder.daysLeft;
        TextView subject = holder.subject;
        final Button linkButton = holder.linkButton;
        LinearLayout tagsContainer = holder.tagsContainer;
        ImageView media = holder.media;
        LinearLayout pollAnswerButtonsContainer = holder.pollAnswerButtonsContainer;
        RelativeLayout extraButtonsContainerOfContainer = holder.extraButtonsContainerOfContainer;
        RelativeLayout extraButtonsContainer = holder.extraButtonsContainer;
        TextView counter = holder.counter;
        RelativeLayout shareButton = holder.shareButton;
        final RelativeLayout lockButton = holder.lockButton;
        ImageView lockImage = holder.lockImage;
        final Button bookMarkButton = holder.bookMarkButton;

        final Poll p = getItem(position);

        //just for demo purposes
        if(position == 0){
            p.setAnswered(1);
        }

        String startDateGreekFormat = Utils.timestampToGreekDateFormat(p.getStartDate());
        String daysLeft = Utils.findDaysDifferenceFromNow(p.getEndDate());

        boolean isAnswered = p.getAnswered() > -1;
        int answerIndex = p.getAnswered();
        boolean isExpired = p.isExpired();
        final int results[] = p.getResults();
        int resultsCount = 0;
        for (int ri = 0, rj = results.length; ri < rj; ri++) {
            resultsCount += results[ri];
        }

        subject.setText(p.getSubject());
        ((TextView) rowView.findViewById(R.id.date)).setText(startDateGreekFormat);
        ((TextView) rowView.findViewById(R.id.daysLeft)).setText(daysLeft);

        Picasso.with(media.getContext()).load(p.getMedia()).into(media);

        bookMarkButton.setBackgroundResource(p.getBookmark() ? R.mipmap.fav_active : R.mipmap.fav);
        bookMarkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bookMarkButton.animate().rotation(bookMarkButton.getRotation() - 360).setDuration(1000);
                //add to bookmarks
            }
        });

        ArrayList<String> tags = p.getTags();

        tagsContainer.removeAllViews();
        for (int i = 0, j = tags.size(); i < j; i++) {
            TextView tagButton = new TextView(rowView.getContext());
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);

            tagButton.setLayoutParams(params);
            tagButton.setPadding(0, 10 * dp, 15 * dp, i == j - 1 ? 0 : 3 * dp);
            tagButton.setText(tags.get(i));
            tagButton.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);
            tagButton.setTextColor(this.context.getResources().getColor(R.color.smallTextGrey));
            tagButton.setCursorVisible(false);
            tagButton.setInputType(InputType.TYPE_CLASS_TEXT);
            tagButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //TODO load list of tag related polls
                }
            });
            tagsContainer.addView(tagButton);
        }

        ArrayList<String> options = p.getAnswers();
        Context c = rowView.getContext();
        final ArrayList<RelativeLayout> buttonContainers = new ArrayList<RelativeLayout>(options.size());
        final ArrayList<View> fillers = new ArrayList<View>(options.size());
        pollAnswerButtonsContainer.removeAllViews();
        for (int i = 0, j = options.size(); i < j; i++) {
            Button optionButton = new Button(rowView.getContext());
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);


            final RelativeLayout buttonContainer = new RelativeLayout(c);
            buttonContainer.setLayoutParams(params);


            final View filler = new View(c);
            filler.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, buttonHeight));
            filler.setBackgroundResource(answerIndex == i ? R.color.pollFillOrange : R.color.greyBG);
            filler.setPivotX(0);
            filler.setScaleX(0);


            ViewGroup.LayoutParams olp = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, buttonHeight);

            optionButton.setLayoutParams(olp);
            optionButton.setText(options.get(i));
            optionButton.setTextColor(this.context.getResources().getColor(R.color.bigTextBlack));
            optionButton.setCursorVisible(false);
            optionButton.setInputType(InputType.TYPE_CLASS_TEXT);
            optionButton.setBackgroundResource(R.color.transparent);

            final int ind = i;

            optionButton.setOnClickListener(new View.OnClickListener() {
                int index = ind;

                @Override
                public void onClick(View v){
                    //vote
                }
            });

            View line = new View(rowView.getContext());
            LinearLayout.LayoutParams lineParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, dp);
            lineParams.setMargins(0, 0, 0, 10 * dp);
            line.setLayoutParams(lineParams);
            line.setBackgroundResource(!isAnswered || answerIndex == i ? R.color.pollFillOrange : R.color.greyBG);

            buttonContainer.addView(filler);
            buttonContainer.addView(optionButton);
            pollAnswerButtonsContainer.addView(buttonContainer);
            pollAnswerButtonsContainer.addView(line);

            fillers.add(filler);
            buttonContainers.add(buttonContainer);


            if (answerIndex > -1 || isExpired) {
                fillRow(buttonContainers.get(i), fillers.get(i), (float) results[i] / (float) resultsCount, this.context.getResources().getColor(i == answerIndex ? R.color.pollFillOrange : R.color.greyBG));

                extraButtonsContainer.setPivotY(0);
                extraButtonsContainer.setTranslationY(-40);
                LinearLayout.LayoutParams eBCCP = (LinearLayout.LayoutParams) extraButtonsContainerOfContainer.getLayoutParams();
                eBCCP.height = buttonHeight;
                extraButtonsContainerOfContainer.setLayoutParams(eBCCP);
                extraButtonsContainerOfContainer.requestLayout();
                extraButtonsContainer.animate().translationY(0).setDuration(500);

                ((TextView) rowView.findViewById(R.id.counter)).setText("" + resultsCount);

                ((RelativeLayout) rowView.findViewById(R.id.shareButton)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent share = new Intent(android.content.Intent.ACTION_SEND);
                        share.setType("text/plain");

                        share.putExtra(Intent.EXTRA_SUBJECT, p.getSubject());
                        share.putExtra(Intent.EXTRA_TEXT, context.getString(R.string.baseurl) + p.getId());

                        context.startActivity(Intent.createChooser(share, context.getString(R.string.share)));
                    }
                });

                if (isExpired) {
                    lockButton.setAlpha(0.3f);
                    lockButton.setTag("expired");
                    lockButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            return;
                        }
                    });
                    ((ImageView) rowView.findViewById(R.id.clockIcon)).setBackgroundResource(R.mipmap.clock_active);
                } else {
                    lockButton.setTag("locked");
                    final int rs = resultsCount;
                    final int ai = answerIndex;
                    lockButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            RelativeLayout b = (RelativeLayout) v;
                            ImageView bb = (ImageView) b.findViewById(R.id.lockImage);

                            String tag = (String) b.getTag();
                            tag = tag.equals("locked") ? "unlocked" : "locked";
                            b.setTag(tag);
                            bb.setBackgroundResource(tag.equals("locked") ? R.mipmap.unlock_pressed : R.mipmap.unlock);
                            for (int i = 0, j = buttonContainers.size(); i < j; i++) {
                                if (tag.equalsIgnoreCase("unlocked")) {
                                    emptyRow(buttonContainers.get(i), fillers.get(i));
                                } else {
                                    fillRow(buttonContainers.get(i), fillers.get(i), (float) results[i] / (float) rs, context.getResources().getColor(i == ai ? R.color.pollFillOrange : R.color.greyBG));
                                }

                            }

                        }
                    });
                }

            }

        }

        String linkText = p.getUrl();
        if (linkText != null && linkText.length() > 0) {
            linkButton.setVisibility(View.VISIBLE);
            linkButton.setText(linkText);
            linkButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent webViewIntent = new Intent(context, WebViewActivity.class);
                    webViewIntent.putExtra("title", "Pipoll");
                    webViewIntent.putExtra("url", linkButton.getText().toString());
                    context.startActivity(webViewIntent);
                }
            });
        } else {
            linkButton.setVisibility(View.GONE);
        }

        return rowView;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }
}
