package giorgos.nl.pipoll_demo;

import java.util.List;

import giorgos.nl.pipoll_demo.models.Poll;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.http.GET;
import retrofit.http.Path;
import rx.Observable;

/**
 * Created by giorgos on 05/07/16.
 */
public class PollsService {


    private static final String BASE_URL = "http://pi.pipoll.net/api";

    private PipollAPI api;

    public PollsService() {


        RequestInterceptor requestInterceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                request.addHeader("Accept", "application/json");
            }
        };

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(BASE_URL)
                .setRequestInterceptor(requestInterceptor)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();

        api = restAdapter.create(PipollAPI.class);
    }

    public PipollAPI getApi() {
        return api;
    }
}
