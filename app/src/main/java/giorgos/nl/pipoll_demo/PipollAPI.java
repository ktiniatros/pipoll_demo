package giorgos.nl.pipoll_demo;

import java.util.List;

import giorgos.nl.pipoll_demo.models.Poll;
import retrofit.http.GET;
import retrofit.http.Path;
import rx.Observable;

public interface PipollAPI {

    @GET("/polls")
    public Observable<List<Poll>>
    getPosts();

    @GET("/polls/{id}")
    public Observable<Poll>
    getPost(@Path("id") int postId);
}
