package giorgos.nl.pipoll_demo;

import android.text.format.DateFormat;
import android.util.Base64;

import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by giorgos on 05/07/16.
 */
public class Utils {
    public static final String timestampToGreekDateFormat(long timestamp){
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(timestamp);
        String date = DateFormat.format("dd/MM/yyyy", cal).toString();
        return date;
    }

    public static final String findDaysDifferenceFromNow(long timestamp){
        long now = Calendar.getInstance().getTimeInMillis();
        Calendar endDate = Calendar.getInstance();
        endDate.setTimeInMillis(timestamp);
        endDate.setTimeZone(TimeZone.getTimeZone("Europe/London"));
        Calendar nowDate = Calendar.getInstance();
        nowDate.setTimeZone(TimeZone.getTimeZone("Europe/London"));
        long daysBetween = 0;
        while (nowDate.before(endDate)) {
            nowDate.add(Calendar.DAY_OF_MONTH, 1);
            daysBetween++;
            if(daysBetween > 19){
                return "19+ ημ";
            }
        }
        return daysBetween + " ημ";
    }

    public static String AESEncrypt(String plaintext) {
        final byte[] KEY = "golddolphinrains".getBytes();

        try {
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            final SecretKeySpec secretKey = new SecretKeySpec(KEY, "AES");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            final String encryptedString = Base64.encodeToString(
                    cipher.doFinal(plaintext.getBytes()), Base64.DEFAULT);

            return encryptedString;
        } catch (Exception e) {
            return "";
        }
    }
}
