package giorgos.nl.pipoll_demo.models;

import com.google.gson.JsonArray;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by giorgos on 05/07/16.
 */
public class Poll {
    private final static String TAG = "p_POLL";
    private int id = 0;

    private long startDate = 0;
    private long endDate = 0;
    private String daysLeft = "";
    private String startDateGreekFormat = "";
    public String subject = "";
    private boolean bookmark = false;
    private String media = "";

    private int answered = -1;

    int[] results;

    ArrayList<String> tags = new ArrayList();
    ArrayList<String> answers = new ArrayList();

    private String url = "";

    public String getSubject(){
        return subject;
    }

    public String getMedia(){
        return media;
    }

    public String getGreekDate(){
        return startDateGreekFormat;
    }

    public String getDaysLeft(){
        return daysLeft;
    }

    public ArrayList<String> getTags(){
        return tags;
    }

    public ArrayList<String> getAnswers(){
        return answers;
    }

    public boolean getBookmark(){
        return bookmark;
    }

    public void setBookmark(boolean b){
        bookmark = b;
    }

    public int getAnswered() {
        return answered;
    }

    public void setAnswered(int answered) {
        this.answered = answered;
    }

    public int[] getResults() {
        return results;
    }

    public void setResults(JsonArray results) {
        for(int i = 0, j = results.size(); i < j; i++){
            this.results[i] = results.get(i).getAsInt();
        }
    }

    public boolean isExpired(){
        Calendar end = Calendar.getInstance();
        end.setTimeInMillis(endDate);
        return  Calendar.getInstance().after(end);
    }

    public int getId(){
        return id;
    }

    public String getUrl() {
        return url;
    }



    public long getStartDate() {
        return startDate;
    }

    public long getEndDate() {
        return endDate;
    }
}
