package giorgos.nl.pipoll_demo.presenters;

import java.util.List;

import giorgos.nl.pipoll_demo.MainActivity;
import giorgos.nl.pipoll_demo.PollsService;
import giorgos.nl.pipoll_demo.models.Poll;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by giorgos on 05/07/16.
 */
public class PollsPresenter {
    private MainActivity pollsListActivity;
    private PollsService pollsService;

    public PollsPresenter(MainActivity list, PollsService service){
        pollsListActivity = list;
        pollsService = service;
    }

    public void refresh(){
        pollsService.getApi().getPosts()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<Poll>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(List<Poll> polls) {
                        pollsListActivity.refreshPollsList(polls);
                    }
                });
    }
}
